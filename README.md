# My Sweet Hero App

An application developed as a coding test. The application has a screen with products downloaded from API service.

Application is written according to MVVM framework using ViewModels and LiveData to communicate
with views.

## Extra features
* Swipe-to-refresh action which updates products from the API.

## Used libraries
* Dagger
* RxJava2
* Retrofit2
* Architecture Components (LiveData, ViewModel,...)
* Other

## Nice to have
* More tests
* API with complete data, so there are no items with missing data (name, brand, size,...).
* Sweet icon.

## Questions
1. How long did you spend on the coding test?
  - 4 hours.
2. What would you add to your solution if you had more time?
  - I would implement more tests and use Kotlin properly. I'm currently using only Java on work related projects and Kotlin only for side projects. That's also a reason it took me so long to finish this task.