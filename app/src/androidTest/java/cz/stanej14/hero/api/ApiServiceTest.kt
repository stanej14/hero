package cz.stanej14.hero.api

import cz.stanej14.hero.core.BaseInstrumentationTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

/**
 * Test if ApiService is returning data.
 * Created by Jan Stanek on {17/04/18}
 **/
class ApiServiceTest : BaseInstrumentationTest() {

    @Inject
    lateinit var apiService: ApiService

    @Before
    fun setUp() {
        testComponent.inject(this)
    }

    @Test
    fun getRecipes() {
        val responses = apiService.getProducts().blockingGet()

        // Not empty response.
        Assert.assertFalse(responses.isEmpty())
    }
}