package cz.stanej14.hero.core

import android.support.test.InstrumentationRegistry

/**
 * TODO add class description
 * Created by Jan Stanek on {22/06/17}
 **/
abstract class BaseInstrumentationTest {

    var testComponent: TestComponent =
            DaggerTestComponent.builder()
                    .appContext(InstrumentationRegistry.getTargetContext())
                    .build()

}