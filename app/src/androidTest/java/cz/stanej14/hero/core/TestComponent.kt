package cz.stanej14.hero.core

import android.content.Context
import cz.stanej14.hero.api.ApiServiceTest
import cz.stanej14.hero.di.AppModule
import cz.stanej14.hero.di.MainActivityModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * TODO add class description
 * Created by Jan Stanek on {22/06/17}
 **/
@Singleton
@Component(modules = arrayOf(
        AndroidInjectionModule::class,
        AppModule::class,
        MainActivityModule::class))
interface TestComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun appContext(appContext: Context): Builder

        fun build(): TestComponent
    }

    fun inject(apiServiceTest: ApiServiceTest)
}