package cz.stanej14.hero.viewmodel;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import cz.stanej14.hero.R;
import cz.stanej14.hero.domain.ProductRepository;
import cz.stanej14.hero.domain.model.Product;
import cz.stanej14.hero.ui.products.ProductListViewModel;
import io.reactivex.Flowable;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Class for testing {@link ProductListViewModel}.
 * <p>
 * Class is in java, because i didn't have time to make Mockito work with Kotlin :(. Still it doesn't
 * work though. This would work in a Java development but here, there is a problem with a lateinit
 * property even though it's mocked and i didn't have time to figure it out.
 * <p>
 * Created by Jan Stanek on {17/04/18}
 **/
public class ProductViewModelTest {
    @Rule
    public TestRule rule = new InstantTaskExecutorRule();

    @Mock
    private ProductRepository repository;

    @Mock
    private Observer<List<Product>> productObserver;

    @Mock
    private Observer<Boolean> progressObserver;

    @Mock
    private Observer<Integer> snackObserver;

    @Before
    public void setUp() {
        System.setProperty(
                "dexmaker.dexcache",
                getInstrumentation().getTargetContext().getCacheDir().getPath());

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void loadData() throws Exception {
        List<Product> list = new ArrayList<>();
        Mockito.when(repository.updateProducts()).thenReturn(Flowable.just(list));

        ProductListViewModel viewModel = new ProductListViewModel(repository);
        viewModel.observeProducts().observeForever(productObserver);
        viewModel.observeProgress().observeForever(progressObserver);
        viewModel.observeSnack().observeForever(snackObserver);

        verify(progressObserver, times(1)).onChanged(true);
        verify(progressObserver, times(1)).onChanged(false);
        verify(snackObserver, never()).onChanged(R.string.products_error_loading);
        verify(productObserver).onChanged(ArgumentMatchers.any());
    }

    @Test
    public void loadDataWithoutInternet() {
        Mockito.when(repository.updateProducts()).thenReturn(Flowable.error(new Throwable()));

        ProductListViewModel viewModel = new ProductListViewModel(repository);
        viewModel.observeProducts().observeForever(productObserver);
        viewModel.observeProgress().observeForever(progressObserver);
        viewModel.observeSnack().observeForever(snackObserver);

        verify(progressObserver, times(1)).onChanged(true);
        verify(progressObserver, times(1)).onChanged(false);
        verify(snackObserver).onChanged(R.string.products_error_loading);
        verify(productObserver, times(1)).onChanged(null);
    }
}
