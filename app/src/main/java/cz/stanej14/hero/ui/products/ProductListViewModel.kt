package cz.stanej14.hero.ui.products

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import cz.stanej14.hero.R
import cz.stanej14.hero.common.livedata.SingleLiveData
import cz.stanej14.hero.domain.ProductRepository
import cz.stanej14.hero.domain.model.Product
import cz.stanej14.hero.domain.model.ProductComparator
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

/**
 * ViewModel for ProductListFragment.
 * Created by Jan Stanek on {17/04/18}
 **/
class ProductListViewModel @Inject constructor(var productRepository: ProductRepository) : ViewModel() {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val showProgress: MutableLiveData<Boolean> = MutableLiveData()
    private val showSnack: MutableLiveData<Int> = SingleLiveData<Int>()
    private val products: MutableLiveData<List<Product>> = MutableLiveData()

    init {
        processProducts(productRepository.getProducts(ProductComparator.Criteria.NAME))
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun observeProgress(): LiveData<Boolean> {
        return showProgress
    }

    fun observeProducts(): LiveData<List<Product>> {
        return products
    }

    fun observeSnack(): LiveData<Int> {
        return showSnack
    }

    /**
     * Updates data from API.
     */
    fun updateData() {
        processProducts(productRepository.updateProducts())
    }

    fun getSorted(criteria: ProductComparator.Criteria) {
        processProducts(productRepository.getProducts(criteria))
    }

    private fun processProducts(productFlowable: Flowable<List<Product>>) {
        compositeDisposable.add(productFlowable.doOnSubscribe({ showProgress.postValue(true) })
                .doFinally({ showProgress.postValue(false) })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(products::postValue, {
                    Timber.e(it)
                    products.postValue(ArrayList())
                    showSnack.postValue(R.string.products_error_loading)
                }))
    }
}