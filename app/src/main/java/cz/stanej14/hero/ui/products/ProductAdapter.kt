package cz.stanej14.hero.ui.products

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import cz.stanej14.hero.R
import cz.stanej14.hero.domain.model.Product


/**
 * Adapter for products.
 * Created by Jan Stanek on {17/04/18}
 **/
class ProductAdapter(var products: List<Product>) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ProductViewHolder(inflater.inflate(R.layout.item_product, parent, false))
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(products[position])
    }

    override fun getItemCount(): Int {
        return products.size
    }

    fun setData(posts: List<Product>) {
        this.products = posts
        notifyDataSetChanged()
    }

    /**
     * ViewHolder for each product.
     */
    class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val txtName: TextView = itemView.findViewById(R.id.txt_product_name)
        private val txtBrand: TextView = itemView.findViewById(R.id.txt_product_brand)
        private val txtId: TextView = itemView.findViewById(R.id.txt_product_id)

        fun bind(product: Product) {
            product.apply {
                txtName.text = name
                txtBrand.text = brand
                txtId.text = id
            }
        }
    }
}