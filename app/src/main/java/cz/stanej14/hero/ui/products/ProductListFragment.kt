package cz.stanej14.hero.ui.products

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import cz.stanej14.hero.R
import cz.stanej14.hero.common.snack
import cz.stanej14.hero.di.Injectable
import cz.stanej14.hero.domain.model.Product
import cz.stanej14.hero.domain.model.ProductComparator
import javax.inject.Inject

/**
 * Fragment with list of products.
 * Created by Jan Stanek on {17/04/18}
 **/
class ProductListFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: ProductListViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var viewEmpty: View
    private var productAdapter: ProductAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.product_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.sort_by_name -> viewModel.getSorted(ProductComparator.Criteria.NAME)
            R.id.sort_by_brand -> viewModel.getSorted(ProductComparator.Criteria.BRAND)
            R.id.sort_by_id -> viewModel.getSorted(ProductComparator.Criteria.ID)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Init UI.
        viewEmpty = view.findViewById(R.id.txt_empty)
        recyclerView = view.findViewById(R.id.recycler_products)
        recyclerView.layoutManager = LinearLayoutManager(context)
        swipeRefresh = view.findViewById(R.id.swipe_products)
        swipeRefresh.setOnRefreshListener { viewModel.updateData() }

        // Init ViewModel.
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductListViewModel::class.java)
        viewModel.observeProgress().observe(this, Observer { swipeRefresh.isRefreshing = it!! })
        viewModel.observeSnack().observe(this, Observer { view.snack(it!!) })
        viewModel.observeProducts().observe(this, Observer { showData(it!!) })
    }

    private fun showData(data: List<Product>) {
        if (data.isEmpty()) {
            viewEmpty.visibility = View.VISIBLE
            return
        }
        viewEmpty.visibility = View.GONE

        if (productAdapter == null) {
            productAdapter = ProductAdapter(data)
            recyclerView.adapter = productAdapter
        } else {
            productAdapter!!.setData(data)
        }
    }
}