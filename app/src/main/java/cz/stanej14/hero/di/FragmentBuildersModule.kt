package cz.stanej14.hero.di

import cz.stanej14.hero.ui.products.ProductListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module to inject fragments.
 * Created by Jan Stanek on {27/06/17}
 **/
@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeProductListFragment(): ProductListFragment
}