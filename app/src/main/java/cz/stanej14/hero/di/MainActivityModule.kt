package cz.stanej14.hero.di

import cz.stanej14.hero.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module to inject main activity.
 * Created by Jan Stanek on {27/06/17}
 **/
@Module
public abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = arrayOf(FragmentBuildersModule::class))
    abstract fun contributeMainActivity(): MainActivity
}