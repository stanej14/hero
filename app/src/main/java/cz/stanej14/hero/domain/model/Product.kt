package cz.stanej14.hero.domain.model

import com.google.gson.annotations.SerializedName

/**
 * POJO class for a product.
 * Created by Jan Stanek on {17/04/18}
 **/
data class Product(
        @SerializedName("gtin14") val id: String,
        @SerializedName("brand_name") val brand: String,
        val name: String
)