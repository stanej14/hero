package cz.stanej14.hero.domain

import android.text.TextUtils
import cz.stanej14.hero.api.ApiService
import cz.stanej14.hero.domain.model.Product
import cz.stanej14.hero.domain.model.ProductComparator
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository for providing products.
 * Created by Jan Stanek on {17/04/18}
 **/
@Singleton
open class ProductRepository @Inject constructor() {

    @Inject
    lateinit var apiService: ApiService
    var productsCash: List<Product>? = null

    /**
     * Gets products either from cash or from web sorted by given criteria.
     */
    fun getProducts(criteria: ProductComparator.Criteria): Flowable<List<Product>> {
        // Get items from cash or from API.
        val toReturn: Flowable<List<Product>> = if (productsCash != null) {
            Flowable.just(productsCash)
        } else {
            updateProducts()
        }

        // Return products in a sorted list.
        val comparator = ProductComparator(criteria)
        return toReturn.flatMap {
            Flowable.fromIterable(it)
                    .toSortedList(comparator::compare)
                    .toFlowable()
        }
    }

    /**
     * Updates products from API.
     */
    fun updateProducts(): Flowable<List<Product>> {
        return apiService.getProducts()
                .toFlowable()
                .flatMap({
                    Flowable.fromIterable(it)
                            .filter({ !TextUtils.isEmpty(it.name) })
                            .filter({ !TextUtils.isEmpty(it.id) })
                            .filter({ !TextUtils.isEmpty(it.brand) })
                            .toList()
                            .toFlowable()
                })
                .doOnNext({ productsCash = it })
    }
}