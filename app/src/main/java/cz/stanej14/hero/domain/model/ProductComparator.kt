package cz.stanej14.hero.domain.model

/**
 * Class responsible for comparing products.
 * Created by Jan Stanek on {18/04/18}
 **/
class ProductComparator(private val criteria: Criteria) {

    enum class Criteria {
        NAME, ID, BRAND
    }

    /**
     * Compares strings lexicographically.
     */
    fun compare(p1: Product, p2: Product): Int {
        return when (criteria) {
            Criteria.NAME -> p1.name.compareTo(p2.name)
            Criteria.ID -> p1.id.compareTo(p2.id)
            Criteria.BRAND -> p1.brand.compareTo(p2.brand)
        }
    }
}