package cz.stanej14.hero

/**
 * Class with constants.
 * Created by Jan Stanek on {17/04/18}
 **/
object Constants {

    // API
    val API_URL = "https://www.datakick.org/api/"
}