package cz.stanej14.hero.api

import cz.stanej14.hero.domain.model.Product
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Api service.
 * Created by Jan Stanek on {17/04/18}
 **/
interface ApiService {

    @GET("items")
    fun getProducts(): Single<List<Product>>
}