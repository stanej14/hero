package cz.stanej14.hero.common

import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.View

/**
 * File with some extensions.
 * Created by Jan Stanek on {17/04/18}
 **/
fun View.snack(@StringRes message: Int, length: Int = Snackbar.LENGTH_LONG) {
    val snack = Snackbar.make(this, message, length)
    snack.show()
}