package cz.stanej14.hero.domain.model

import org.junit.Assert.assertTrue
import org.junit.Test

/**
 * Test for ProductComparator.
 * Created by Jan Stanek on {18/04/18}
 */
class ProductComparatorTest {

    @Test
    fun compareByName() {
        val p1 = Product("id", "brand", "aaa")
        val p2 = Product("id", "brand", "bbb")
        val comparator = ProductComparator(ProductComparator.Criteria.NAME)
        assertTrue(comparator.compare(p1, p2) < 1)
    }

    @Test
    fun compareByBrand() {
        val p1 = Product("id", "aaaa", "aaa")
        val p2 = Product("id", "bbbb", "bbb")
        val comparator = ProductComparator(ProductComparator.Criteria.BRAND)
        assertTrue(comparator.compare(p1, p2) < 1)
    }

    @Test
    fun compareById() {
        val p1 = Product("0", "brand", "aaa")
        val p2 = Product("1", "brand", "bbb")
        val comparator = ProductComparator(ProductComparator.Criteria.ID)
        assertTrue(comparator.compare(p1, p2) < 1)
    }
}